import Student from '../entity/student';
import { Observable } from 'rxjs';

export abstract class StudentService {
     abstract getStudents(): Observable<Student[]>;
     abstract getStudent(id: number): Observable<Student>;
}
